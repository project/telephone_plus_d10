<?php

namespace Drupal\telephone_plus\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telephone_plus\TelephonePlusFormatter;
use Drupal\telephone_plus\TelephonePlusValidator;

/**
 * Plugin implementation of the 'telephone_plus_link' formatter.
 *
 * @FieldFormatter(
 *   id = "telephone_plus_link",
 *   label = @Translation("TelephonePlus link"),
 *   description = @Translation("Formats telephone fields as text and link."),
 *   field_types = {
 *     "telephone_plus_field"
 *   }
 * )
 */
class TelephonePlusLinkFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();

    $options['vcard'] = TRUE;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['vcard'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable vCard support'),
      '#default_value' => $this->getSetting('vcard'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $vcard_enabled = $this->getSetting('vcard');

    $summary[] = $this->t('vCard enabled: %enabled', ['%enabled' => ($vcard_enabled) ? 'Yes' : 'No']);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $title = NULL;
      $supplementary = NULL;

      $telephone = new TelephonePlusValidator($item->telephone_number, $item->telephone_extension, $item->country_code);

      // If we don't have a valid number, set variables to allow fallback to
      // plain text.
      if (!$telephone->isValid()) {
        $telephone_text = $item->telephone_number;
        $telephone_link = '';
      }
      else {
        $telephone = new TelephonePlusFormatter($item->telephone_number, $item->telephone_extension, $item->country_code);
        // TelephonePlus link text.
        $telephone_link = $telephone->url();
        // TelephonePlus display text.
        $telephone_text = $telephone->text($item->display_international_number);
      }

      // Add extension to TelephonePlus display text if there is one.
      if ($item->telephone_extension) {
        $telephone_text .= ' ' . t('ext. :extension', [':extension' => $item->telephone_extension]);
      }

      if (!empty($item->telephone_title)) {
        $title = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => ['class' => ['title']],
          '#value' => $item->telephone_title,
        ];

        // Add vCard title if enabled.
        if ($this->getSetting('vcard')) {
          $title['#attributes']['class'][] = 'type';
        }
      }

      if (!empty($item->telephone_supplementary)) {
        $supplementary = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => ['class' => ['supplementary']],
          '#value' => $item->telephone_supplementary,
        ];
      }

      if (!empty($telephone_link)) {
        // Url::fromUri() doesn't place nice with the generated tel: URI
        // so reverting to using an html_tag instead of a link element.
        $phone = [
          '#type' => 'html_tag',
          '#tag' => 'a',
          '#attributes' => ['href' => $telephone_link],
          '#value' => $telephone_text,
        ];
      }
      else {
        $phone = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $telephone_text,
        ];
      }

      if ($item->telephone_extension) {
        $phone['#suffix'] = ' ' . t('extension  :extension', [':extension' => $item->telephone_extension]);
      }

      // If vCard option is enabled and add required classes and
      // enclose in div element with 'tel' class.
      if ($this->getSetting('vcard')) {
        $phone['#attributes']['class'][] = 'value';
        if (!isset($item->_attributes)) {
          $item->_attributes = [];
        }
        $item->_attributes += ['class' => ['tel']];
      }

      if ($title) {
        $elements[$delta]['title'] = $title;
      }

      if ($supplementary) {
        $elements[$delta]['supplementary'] = $supplementary;
      }

      $elements[$delta]['number'] = $phone;
    }

    return $elements;
  }

}
