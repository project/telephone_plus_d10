<?php

namespace Drupal\telephone_plus\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telephone_plus\TelephonePlusFormatter;

/**
 * Plugin implementation of the 'telephone_plus_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "telephone_plus_plain",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "telephone_plus_field"
 *   }
 * )
 */
class TelephonePlusPlainFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $title = NULL;
      $supplementary = NULL;

      // TelephonePlus display text.
      $telephone = new TelephonePlusFormatter($item->telephone_number, $item->telephone_extension, $item->country_code);
      $telephone_text = $telephone->text($item->display_international_number);

      if (!empty($item->telephone_title)) {
        $title = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => ['class' => ['title']],
          '#value' => $item->telephone_title,
        ];
      }

      if ($item->telephone_extension) {
        $telephone_text .= ' ' . t('extension  :extension', [':extension' => $item->telephone_extension]);
      }

      $phone = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => ['number']],
        '#value' => $telephone_text,
      ];

      if (!empty($item->telephone_supplementary)) {
        $supplementary = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => ['class' => ['supplementary']],
          '#value' => $item->telephone_supplementary,
        ];
      }

      if ($title) {
        $elements[$delta]['title'] = $title;
      }

      if ($supplementary) {
        $elements[$delta]['supplementary'] = $supplementary;
      }

      $elements[$delta]['number'] = $phone;

    }

    return $elements;
  }

}
