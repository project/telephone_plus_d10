<?php

namespace Drupal\telephone_plus\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\telephone_plus\TelephonePlusFormatter;
use Drupal\telephone_plus\TelephonePlusValidator;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;

/**
 * Plugin implementation of the 'telephone_plus_widget' widget.
 *
 * @FieldWidget (
 *   id = "telephone_plus_widget",
 *   label = @Translation("Telephone Plus widget"),
 *   field_types = {
 *     "telephone_plus_field"
 *   }
 * )
 */
class TelephonePlusWidget extends WidgetBase {

  /**
   * PhoneNumber Util definition.
   *
   * @var \libphonenumber\PhoneNumberUtil
   */
  protected $phoneNumberUtil;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->phoneNumberUtil = PhoneNumberUtil::getInstance(PhoneNumberUtil::META_DATA_FILE_PREFIX);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = $this->fieldDefinition->getSettings();

    if ($settings['title_enabled']) {
      $element['telephone_title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => $items[$delta]->telephone_title ?? NULL,
      ];
    }

    $element['telephone_container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['container-inline']],
    ];

    if (!$settings['country_code_enabled'] || $this->isDefaultValueWidget($form_state)) {
      $element['telephone_container']['country_code'] = [
        '#type' => 'hidden',
        '#size' => 2,
        '#default_value' => $items[$delta]->country_code ?? $settings['default_country_code'],
      ];
    }
    else {
      $element['telephone_container']['country_code'] = [
        '#type' => 'select',
        '#title' => 'Country',
        '#label_attributes' => ['style' => ['display: block']],
        '#options' => CountryManager::getStandardList(),
        '#default_value' => $items[$delta]->country_code ?? $settings['default_country_code'],
      ];
    }

    $element['telephone_container']['telephone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#label_attributes' => ['style' => ['display: block']],
      '#default_value' => $items[$delta]->telephone_number ?? NULL,
    ];

    $element['telephone_container']['telephone_extension'] = [
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => $this->t('Extension'),
      '#wrapper_attributes' => ['class' => ['container-inline']],
      '#default_value' => $items[$delta]->telephone_extension ?? NULL,
    ];

    if ($settings['supplementary_enabled']) {
      $element['telephone_supplementary'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Supplementary'),
        '#default_value' => $items[$delta]->telephone_supplementary ?? NULL,
        '#description' => $this->t('Additional information such as operating hours.'),
      ];
    }

    $element['display_international_number'] = [
      '#type' => 'checkbox',
      '#title' => 'Display as international dialing number format',
      '#default_value' => $items[$delta]->display_international_number ?? NULL,
    ];

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += [
        '#type' => 'fieldset',
        '#attributes' => ['class' => ['container-inline']],
      ];
    }
    $element['#element_validate'][] = [static::class, 'validateTelephoneNumber'];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['country_code'] = $value['telephone_container']['country_code'];
      $value['telephone_number'] = $value['telephone_container']['telephone_number'];
      $value['telephone_extension'] = $value['telephone_container']['telephone_extension'];
      unset($value['telephone_container']);
    }

    return $values;
  }

  /**
   * Form validation handler for the 'number' element.
   */
  public static function validateTelephoneNumber(&$element, FormStateInterface $form_state, $form) {
    $field = $element['telephone_container'];

    $country_code = $field['country_code']['#value'];
    $number = $field['telephone_number']['#value'];
    $extension = $field['telephone_extension']['#value'];

    if (!empty($number)) {
      $telephone = new TelephonePlusValidator($number, $extension, $country_code);

      if (!$telephone->isValid()) {
        $form_state->setErrorByName(implode('][', $element['#parents']), t('Invalid telephone number.'));
      }
    }
  }

}
