<?php

namespace Drupal\telephone_plus\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use libphonenumber\PhoneNumberUtil as PhoneNumberUtil;
use Drupal\Core\Locale\CountryManager;

/**
 * Plugin implementation of the 'telephone_plus_field' field type.
 *
 * @FieldType(
 *   id = "telephone_plus_field",
 *   label = @Translation("Telephone Plus"),
 *   description = @Translation("Stores Telephone Plus field data"),
 *   default_widget = "telephone_plus_widget",
 *   default_formatter = "telephone_plus_link"
 * )
 */
class TelephonePlusField extends FieldItemBase {

  /**
   * Phone Number Util definition.
   *
   * @var \libphonenumber\PhoneNumberUtil
   */
  protected $phoneNumberUtil;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);

    $this->phoneNumberUtil = PhoneNumberUtil::getInstance(PhoneNumberUtil::META_DATA_FILE_PREFIX);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return parent::defaultStorageSettings() + [
      'title_enabled' => FALSE,
      'supplementary_enabled' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return parent::defaultFieldSettings() + [
      'default_country_code' => NULL,
      'country_code_enabled' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // TODO: Prevent early t() calls by using the TranslatableMarkup.
    $properties['telephone_title'] = DataDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t("Title of the telephone number e.g. 'Customer services'"));

    $properties['country_code'] = DataDefinition::create('string')
      ->setLabel(t('Country code'))
      ->setRequired(TRUE);

    $properties['telephone_number'] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setRequired(TRUE);

    $properties['telephone_extension'] = DataDefinition::create('string')
      ->setLabel(t('Extension'));

    $properties['telephone_supplementary'] = DataDefinition::create('string')
      ->setLabel(t('Supplementary'))
      ->setDescription(t('Supplementary information such as operating times.'));

    $properties['display_international_number'] = DataDefinition::create('boolean')
      ->setLabel(t('Display international dialing number'))
      ->setDescription('Displays the full telephone number including the country dialing code prefix.')
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $settings = $field_definition->getSettings();

    $schema = [
      'columns' => [
        'country_code' => [
          'type' => 'varchar',
          'length' => 2,
        ],
        'telephone_number' => [
          'type' => 'varchar',
          'length' => 60,
        ],
        'telephone_extension' => [
          'type' => 'varchar',
          'length' => 20,
        ],
        'display_international_number' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
          'default' => 0,
        ],
      ],
    ];

    if ($settings['title_enabled']) {
      $schema['columns']['telephone_title'] = [
        'type' => 'varchar',
        'length' => 255,
      ];
    }

    if ($settings['supplementary_enabled']) {
      $schema['columns']['telephone_supplementary'] = [
        'type' => 'varchar',
        'length' => 255,
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $settings = $this->getSettings();
    $element = [];

    $element['title_enabled'] = [
      '#type' => 'checkbox',
      '#title' => 'Enable title field',
      '#default_value' => $settings['title_enabled'],
    ];

    $element['supplementary_enabled'] = [
      '#type' => 'checkbox',
      '#title' => 'Enable supplementary field',
      '#default_value' => $settings['supplementary_enabled'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $settings = $this->getSettings();

    $element['default_country_code'] = [
      '#type' => 'select',
      '#title' => 'Default country code',
      '#label_attributes' => ['style' => ['display: block']],
      '#options' => CountryManager::getStandardList(),
      '#required' => TRUE,
      '#default_value' => $settings['default_country_code'] ?? NULL,
    ];

    $element['country_code_enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow selection of a country dialing code.'),
      '#default_value' => $settings['country_code_enabled'] ?? FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->getValue();
    return empty($value['telephone_number']);
  }

}
